package com.poznyak.oop;

import java.util.ArrayList;

import com.poznyak.oop.cars.Audi;
import com.poznyak.oop.cars.Car;
import com.poznyak.oop.cars.BMW;
import com.poznyak.oop.taxipark.TaxiPark;

public class Main {
    public static void main(String[] args) {
        ArrayList<Car> cars = new ArrayList<Car>();

        cars.add(new Car("Fiat", 3200, 9.5f));
        cars.add(new Car("Toyota", 8450, 7.3f));
        cars.add(new Car("Renault", 3500, 9.4f));
        cars.add(new Car("Volkswagen", 8450, 7.3f));

        cars.add(new Audi(5700, 10.6f));

        cars.add(new BMW(6500, 8.3f));

        TaxiPark taxiPark = new TaxiPark(cars);
        int cost = taxiPark.getCostOfAllCars();
        System.out.println("Стоимость таксопарка: " + cost);
        System.out.println();

        System.out.println("Автомобили, отсортированные по расходу топлива:");
        ArrayList<Car> sortedCars = taxiPark.sortCarsByFuelConsumption();
        for (Car car: sortedCars) {
            System.out.println(car);
        }
        System.out.println();

        int minCost = 6300;
        System.out.println("Автомобили стоимостью более " + minCost + ":");
        ArrayList<Car> filterCarsByCost = taxiPark.filterCars(minCost);
        for (Car car: filterCarsByCost) {
            System.out.println(car);
        }
        System.out.println();

        float minFuelConsumption = 9f;
        System.out.println("Автомобили с расходо топлива более " + minFuelConsumption + ":");
        ArrayList<Car> filteredCarsByFuelConsumption = taxiPark.filterCars(minFuelConsumption);
        for (Car car: filteredCarsByFuelConsumption) {
            System.out.println(car);
        }
        System.out.println();

        Car car = taxiPark.getCarByCostAndFuelConsumption(3700, 9.5f);
        if (car != null) {
            System.out.println("Ваша машина: " + car);
        } else {
            System.out.println("Машина не найдена");
        }
    }
}