package com.poznyak.oop.taxipark;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.poznyak.oop.cars.Car;

public class TaxiPark {
    private ArrayList<Car> cars;

    public TaxiPark(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public int getCostOfAllCars() {
        int total = 0;

        for (Car car : this.cars) {
            total += car.getCost();
        }
        return total;
    }

    public ArrayList<Car> sortCarsByFuelConsumption() {
        Collections.sort(this.cars, new Comparator<Car>() {
            public int compare(Car one, Car other) {
                return Float.compare(one.getFuelConsumption(), other.getFuelConsumption());
            }
        });
        return this.cars;
    }

    public ArrayList<Car> filterCars(float minFuelConsumption) {
        ArrayList<Car> filterCarsByFuelConsumption = new ArrayList<Car>();
        for (Car cars : this.cars) {
            if (cars.getFuelConsumption() > minFuelConsumption) {
                filterCarsByFuelConsumption.add(cars);
            }
        }
        return filterCarsByFuelConsumption;
    }

    public ArrayList<Car> filterCars(int minCost) {
        ArrayList<Car> filterCarsByCost = new ArrayList<Car>();
        for (Car cars : this.cars) {
            if (cars.getCost() > minCost) {
                filterCarsByCost.add(cars);
            }
        }
        return filterCarsByCost;
    }

    public Car getCarByCostAndFuelConsumption(int cost, float fuelConsumption) {
        for (Car car : this.cars) {
            if (cost == car.getCost() && fuelConsumption == car.getFuelConsumption()) {
                return car;
            }
        }

        return null;
    }
}
