package com.poznyak.oop.cars;

public class BMW extends Car {
    public BMW(int cost, float fuelConsumption) {
        super("BMW", cost, fuelConsumption);
    }
}
