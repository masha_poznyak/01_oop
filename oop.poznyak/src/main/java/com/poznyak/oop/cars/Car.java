package com.poznyak.oop.cars;

public class Car {
    private int cost;
    private String name;
    private float fuelConsumption;

    public Car(String name, int cost, float fuelConsumption) {
        this.name = name;
        this.cost = cost;
        this.fuelConsumption = fuelConsumption;
    }

    public int getCost() {
        return this.cost;
    }

    public String getName() {
        return this.name;
    }

    public float getFuelConsumption() {
        return this.fuelConsumption;
    }

    public String toString() {
        return String.format(getName() + " Стоимость:  " + getCost() + " Расход топлива: " + getFuelConsumption());
    }
}
