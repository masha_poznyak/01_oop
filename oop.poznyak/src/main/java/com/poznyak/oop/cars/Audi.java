package com.poznyak.oop.cars;

public class Audi extends Car {
    public Audi(int cost, float fuelConsumption) {
        super("Audi", cost, fuelConsumption);
    }
}
